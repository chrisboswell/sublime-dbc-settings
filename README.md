# README #

### What is this repository for? ###

* DBC language syntax highlighting and settings for Sublime 3.0
* Version 0.4

### How do I get set up? ###

Create a dbc directory in sublime packages directory (Preferences -> Browse Packages)
Copy to the files to the dbc directory


## TODO ##

- fix invalid continue/endif etc

- Indentation defaults
- Trap functions

- Find a way to override goto-definition word seperators
	Will likely need to create a macro/custom command to replace F12 so that it selects the entire word instead of seperating it by '.'
	https://forum.sublimetext.com/t/problem-with-goto-defintion/26228/6